Requires `mdp` installed

```
sudo apt install mdp
```

or

```
sudo dnf install mdp
``` 

Run with `mdp production.md`
