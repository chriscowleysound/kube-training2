%titlee: Deploy a production ready cluster
%author: Chris Cowley

-> Production Ready Cluster <-
===========================

---

-> # Create Cluster <-


    gcloud container clusters create \
      --enable-ip-alias --enable-autoupgrade --enable-autorepair \
      --metadata disable-legacy-endpoints=true --region=europe-west1 \
      --enable-autoscaling --min-nodes=1 --max-nodes=3 \
      --preemptible \
      --scopes=default,cloud-platform \
      <cluster-name>

Creates an autoscaling cluster in Belgium with permissions added to update CloudDNS

---

-> # Install Helm <-

```
wget https://get.helm.sh/helm-v3.3.4-linux-amd64.tar.gz
tar xvf helm-v3.3.4-linux-amd64.tar.gz
cp linux-amd64/helm <somewhere in ${PATH}>
```

---

-> # Clone repo <-

    git clone https://gitlab.com/chriscowleysound/kube-training2.git
---

-> # External DNS <-

```
kubectl apply -k external-dns/overlays/prod
```

---

-> # Ingress Controller <-

    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm upgrade nginx ingress-nginx/ingress-nginx \
        --install \
        --namespace=tools

---

-> # Monitoring <-

Update `monitoring/prometheus-operator-global.yaml` as needed


    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo add stable https://charts.helm.sh/stable
    helm upgrade --install prometheus-stack prometheus-community/kube-prometheus-stack \
      --namespace=monitoring --values=monitoring/prometheus-stack-global.yaml \
      --create-namespace

Does many things:

- Creates an operator to manage Prometheus installs
- Adds CustomResourceDefinitions for Prometheus/AlertManager and other things
- Creates:
  - A Prometheus
  - An Alertmanager
  - Creates some PrometheusRules and ServiceMonitors

---
-> # Logging <-

    helm repo add elastic https://helm.elastic.co
    helm install elastic-operator elastic/eck-operator -n elastic-system --create-namespace
    kubectl create namespace logging
    kubectl apply -f logging/elasticsearch.yaml

---

-> ## Logging tools <-

# Kibana

    kubectl apply -f logging/kibana.yaml

# Cerebro

    helm repo add ccowley https://helm.cowley.tech/
    ES_PASS=$(kubectl --namespace=logging get secret logging-es-elastic-user \
      -o go-template='{{.data.elastic | base64decode }}')
    helm upgrade --install dashboard-es ccowley/cerebro \
      --namespace=logging \
      --install \
      --values=logging/cerebro.yaml \
      --set config.hosts[0].auth.username="elastic" \
      --set config.hosts[0].auth.password="${ES_PASS}"

---
-> ## More Logging tools <-

# Fluentd

    ES_PASS=$(kubectl --namespace=logging get secret logging-es-elastic-user \
      -o go-template='{{.data.elastic | base64decode }}')
    helm repo add kokuwa https://kokuwaio.github.io/helm-charts
    helm upgrade log-forwarder kokuwa/fluentd-elasticsearch \
      --namespace=logging --create-namespace \
      --install \
      --version=${FLUENTD_CHART_VERSION} \
      --values=logging/fluentd.yaml \
      --set elasticsearch.auth.user=${ES_USER} \
      --set elasticsearch.auth.password=${ES_PASS}

---

-> # Gitlab Runner <-

Allows CI to be run within the cluster

    helm repo add gitlab https://charts.gitlab.io
    helm upgrade --install gitlab-runner --namespace=tools gitlab/gitlab-runner
    --set runnerRegistrationToken=${GITLAB_RUNNER_REGISTRATION_TOKEN}
    --values gitlab-runner.yaml

---

-> # 2048 <-

    kubectl apply -f 2048/2048-namespace.yaml \
    -f 2048/2048-deployment.yaml \
    -f 2048/2048-service.yaml \
    -f 2048/2048-ingress.yaml
    

---

-> # Online boutique <-

| Frontend                | Golang  |                |
| cartservice             | C#      | Requires Redis |
| productcatalogueservice | Golang  |                |
| currencyservice         | node.js |                |
| paymentservice          | node.js |                |
| shippingservice         | Golang  |                |
| emailservice            | Python  |                |
| checkoutservice         | Golang  |                |
| recommendationservice   | Python  |                |
| adsservice              | Java    |                |
| recommendationservice   | Python  |                |

---

                             +----------------+       +----------+            +------+
                             | Recommendation |       | frontend +----------->+ cart | 
                             +----------------+       ++--+---+--+            +---+--+
                                                       |  |   |                   |
                                   +----------+        |  |   |                   v
                                   | Checkout +<-------+  |   |              +----+---+
                                   +--------+-+           |   +----------+   | Redis  |
                                            |             |              |   +--------+
                                            |             v              v
                                            |      +------+--------+   +-+--------------+
                                            +----->| Catalogue     +<--+ Recommendation |
                                                   +---------------+   +----------------+

---

-> ## Deployment <-

    helm upgrade redis bitnami/redis --namespace=onlineboutique \
        --install --values=onlineboutique/values/redis/values.yaml
    kubectl apply -k onlineboutique/frontend/base
    kubectl apply -k onlineboutique/adservice/base
    kubectl apply -k onlineboutique/cartservice/base
    kubectl apply -k onlineboutique/checkoutservice/base
    kubectl apply -k onlineboutique/currencyservice/base
    kubectl apply -k onlineboutique/paymentservice/base
    kubectl apply -k onlineboutique/productcatalogservice/base
    kubectl apply -k onlineboutique/recommendationservice/base
    kubectl apply -k onlineboutique/shippingservice/base

    kubectl --namespace=onlineboutique port-forward service/frontend 8080:80



















